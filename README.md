# jk scroll

This is a fork, original project can be found here: [browser](https://addons.mozilla.org/en-US/firefox/addon/jk-scroll/) by [@h43z](https://twitter.com/h43z). 

Shortcut list
```
// content-script.js
j => scroll down
k => scroll up
h => go back one page in history
l => go forward one page in history
Escape => escape from any focus and return to 'vim' mode
```

Run `sh create-extension.sh` and install `jk-extension.zip` manually in your
browsers addons/extension section.

